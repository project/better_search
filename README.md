# Better Search Block
Better Search Block is a light-weight module that changes your boring
Drupal search box into a nice looking search box with icon animations.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/better_search).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/better_search).


## Contents of this file

- Requirements
- Installation
- Configuration


## Requirements

This module requires no modules outside of Drupal core.


## Installation

- Extract the files in your module directory (typically /modules)
- Visit the extend page and enable the module
- From the modules page you will find links to
    - permission settings
    - configuration
    - help


## Configuration

1. Enable the module using drush or by going to `admin/modules`.
1. Go to `/admin/config/search/better-search` for configurations related to
   better search block module.
